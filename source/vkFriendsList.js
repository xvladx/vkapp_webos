enyo.kind({
    name: "MyApps.vkFriendsList",
    kind: enyo.VFlexBox,
    events: {

    },
    published: {
        vk: {}
    },
    components: [
        //Menu
        /*
        { name: "flAppMenu", kind: "AppMenu", components: [
            {caption: "Выйти", onclick: 'logOut'},
            {caption: "О программе"}
        ]},*/

        {
            name: "vkRequest", kind: "WebService",
            onSuccess: "successVkRequest",
            onFailure: "failureVkRequest"
        },

        {
            name: "header", kind: "PageHeader", components: [
            {content: "vkApp for webOS", name: "headerContent"}
        ]
        },
        {kind: "Scroller", flex: 1, components: [
            {name: "list", kind: "VirtualRepeater", onSetupRow: "getListItem",
                components: [
                    {kind: "Item", layoutKind: "VFlexLayout",
                        components: [
                            {name: "title", kind: "Divider"},
                            {name: "content", kind: "HtmlContent",
                                onLinkClick: "doLinkClick"}
                        ],
                        onclick: "listItemClick"
                    }
                ]
            }
        ]}
    ],

    /**
     * Служебные методы
     */
    // openAppMenuHandler: function() {
    //     this.$.flAppMenu.open();
    // },
    // closeAppMenuHandler: function() {
    //     this.$.flAppMenu.close();
    // },
    /**
     * END служебные методы
     */





    create: function () {
        this.inherited(arguments);
        this.vk.friends = [];
    },

    //VkRequest
    successVkRequest: function (inSender, inResponse) {
        console.log(enyo.json.stringify(inResponse));
        this.vk.friends = inResponse.response.items;
        this.$.list.render();
    },
    failureVkRequest: function (inSender, inResponse) {
        console.log("Request failed!");
    },

    testList: function() {
        console.log(this.getVk());
    },
    vkChanged: function () {
        console.log("VK CHANGED!");
        this.$.headerContent.setContent("vkApp for webOS ("+this.vk.screen_name+")");
        this.getVkFriends();
    },

    //Получаем друзей
    getVkFriends: function() {
        this.$.vkRequest.setUrl(this.vk.apiBaseUrl.replace("{METHOD_NAME}", "friends.get")
            .replace("{PARAMETERS}", "order=name&count=10&fields=city,domain,photo_50&name_case=nom"));
        this.$.vkRequest.call();
    },

    getListItem: function(inSender, inIndex) {
        var r = this.vk.friends[inIndex];
        var online = "Offline";
        if (r) {
            if(r.online === 1) {
                online = "Online";
            }
            this.$.title.setCaption(r.first_name+" "+r.last_name);
            this.$.content.setContent("<span><img src=\""+r.photo_50+"\">"+online+"</span>");
            return true;
        }
    }
});
