enyo.kind({
    name: "MyApps.vkAuth",
    kind: enyo.VFlexBox,
    events: {
        onAuth: "",
        onLogOut: ""
    },
    components: [
        //Menu
        {kind: "AppMenu", components: [
            {caption: "Выйти", onclick: "logOut"},
            {caption: "О программе"}
        ]},

        {
            name: "vkRequest", kind: "WebService",
            onSuccess: "successVkRequest",
            onFailure: "failureVkRequest"
        },

        {
            kind: "PageHeader", components: [
            {content: "vkApp for webOS"}
        ]
        },

        {kind: "RowGroup", caption: "Данные для входа", components: [
            {kind: "InputBox", components: [
                {name: "Token", kind: "Input", flex: 1},
                {kind: "Button", caption: "Войти", onclick: "vkLogin"}
            ]},
            {kind: "Button", caption: "Получить токен", onclick: "vkGetToken"}
        ]},
        {name: "errorDialog", kind: "ModalDialog", caption: "Ошибка авторизации"}
    ],

    /**
     * Служебные методы
     */
    openAppMenuHandler: function() {
        this.$.appMenu.open();
    },
    closeAppMenuHandler: function() {
        this.$.appMenu.close();
    },
    /**
     * END служебные методы
     */





    create: function () {
        this.inherited(arguments);
        this.results = [];
        this.vk = {
            data: {},
            appPermissions: [
                'friends',
                'photos',
                'video',
                'pages',
                'messages',
                'wall'
            ],
            clientId: 6087233,
            secretKey: "Idhc99mh4slOJSe2B6cd",
            apiVersion: "5.65",
            token: null,
            apiBaseUrl: "https://api.vk.com/method/{METHOD_NAME}?{PARAMETERS}&access_token={ACCESS_TOKEN}&v=5.65",
            screen_name: null
        };
    },
    vkLogin: function(token) {
        console.log(typeof token);
        if(typeof token === "object") {
            var tokenURL = this.parseUrl(this.$.Token.getValue());
            this.vk.token = tokenURL['#access_token'];
        } else {
            this.vk.token = token;
        }
        this.vk.apiBaseUrl = this.vk.apiBaseUrl.replace("{ACCESS_TOKEN}", this.vk.token);

        this.$.vkRequest.setUrl(this.vk.apiBaseUrl.replace("{METHOD_NAME}", "account.getProfileInfo")
            .replace("{PARAMETERS}", "1=1"));
        this.$.vkRequest.call();
    },
    vkGetToken: function() {
        var url = "https://oauth.vk.com/authorize?client_id="+this.vk.clientId
            +"&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope="+this.vk.appPermissions.join(",")
            +"&response_type=token&v="+this.vk.apiVersion
            +"&revoke=1";
        window.open(url, "VkAuth");
    },

    //Парсер URL
    parseUrl: function (url) {
        var parser = document.createElement("a");
        parser.href = url;
        var result = {};
        parser.hash.split("&").forEach(function (part) {
            var item = part.split("=");
            result[item[0]] = decodeURIComponent(item[1]);
        });
        return result;
    },

    //VkRequest
    successVkRequest: function (inSender, inResponse) {
        console.log("Response got");
        console.log(enyo.json.stringify(inResponse));

        if(typeof inResponse.response !== "undefined") {
            this.vk.screen_name = inResponse.response.screen_name;
            console.log("Successfully authorized "+this.vk.screen_name);
            this.doAuth(this.vk);
        } else if(typeof inResponse.error !== "undefined") {
            this.$.errorDialog.components = [
                {content: inResponse.error.error_msg},
                {kind: "Button", caption: "OK", onclick: "closeErrorDialog"}
            ];
            this.$.errorDialog.openAtCenter();
        }
    },
    closeErrorDialog: function (inSender, inResponse) {
        this.$.errorDialog.close();
    },
    failureVkRequest: function (inSender, inResponse) {
        console.log("Request failed!");
    },
    logOut: function () {
        console.log("Will logOut");
        this.doLogOut();
    }
});
