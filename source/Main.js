enyo.kind({
    name: "MyApps.Main",
    kind: enyo.VFlexBox,
    components: [
        {
            kind: "DbService", dbKind: "ru.darkdev.vkProfile:1", onFailure: "dbFailure", components: [
            {name: "putVk", method: "put", onSuccess: "dbSuccess"},
            {name: "PutVkKind", method: "putKind", onSuccess: "putVkKindSuccess"},
            {name: "deleteVkKind", method: "delKind", onSuccess: "deleteVkKindSuccess"},
            {name: "vkFind", method: "find", onSuccess: "vkFindSuccess"},
            {name: "vkLogout", method: "find", onSuccess: "vkLogoutSuccess"},
            {name: "vkDel", method: "del", onSuccess: "delVkSuccess"},
            {name: "vkClean", method: "del", onSuccess: "delCleanSuccess"}
        ]
        },
        {
            name: "pane", kind: "Pane", flex: 1, onSelectView: "viewSelected",
            components: [
                {
                    name: "loading", className: "enyo-bg", kind: "SpinnerLarge"
                },
                {
                    name: "vkAuth", className: "enyo-bg", kind: "MyApps.vkAuth",
                    onAuth: "vkAuthenticated", onLogOut: "logOut"
                },
                {
                    name: "vkFriendsList", className: "enyo-bg", kind: "MyApps.vkFriendsList"
                }
            ]
        }
    ],
    create: function () {
        this.inherited(arguments);
        this.vk = {};

        //Создаем тип записей в DB
        // this.$.deleteVkKind.call(); // Delete kind
        this.makeVkKind();
        // this.$.pane.selectViewByName("vkAuth");
        this.$.vkFind.call({query:{"from":"ru.darkdev.vkProfile:1"}});
    },
    vkAuthenticated: function (inSender, inVk) {
        this.vk = inVk;
        console.log("VK AUTHENTICATED!");
        this.saveVkToken();
        console.log("Will switch to audios list");
        this.$.vkFriendsList.setVk(inVk);
        this.$.pane.selectViewByName("vkFriendsList");
    },
    makeVkKind: function () {
        var indexes = [{"name": "screen_name", props: [{"name": "screen_name"}]}];
        this.$.PutVkKind.call({owner: enyo.fetchAppId(), indexes: indexes}); // Create db8 kind
    },
    saveVkToken: function () {
        var data = {
            _kind: "ru.darkdev.vkProfile:1", screen_name: this.vk.screen_name, token: this.vk.token
        };
        this.$.putVk.call({objects: [data]});
        console.log("Vk object put");
    },
    vkFindSuccess: function (inSender, inResponse) {
        console.log("vkFindSuccess");
        if(inResponse.results.length === 0) {
            console.log("0 results with token found in db");
            this.$.pane.selectViewByName("vkAuth");
        } else {
            console.log("Previously saved token found!");
            console.log(enyo.json.stringify(inResponse.results[0]));
            this.$.pane.selectViewByName("loading");
            this.$.vkClean.call({ids:[inResponse.results[0]._id]});
            this.$.vkAuth.vkLogin(inResponse.results[0].token);
        }
        console.log(enyo.json.stringify(inResponse));
    },
    deleteVkKindSuccess: function (inSender, inResponse) {
        console.log("Kind deleted!");
    },
    logOut: function () {
        console.log("Will logOut");
        this.$.vkLogout.call({query:{"from":"ru.darkdev.vkProfile:1", "where":[{"prop":"screen_name","op":"=","val":this.vk.screen_name}]}});
        this.vk = {};
    },
    delVkSuccess: function () {
        console.log("Object deleted");
        this.$.pane.selectViewByName("vkAuth");
    },
    vkLogoutSuccess: function(inSender, inResponse) {
        console.log("Will delete object "+inResponse.results[0]._id);
        this.$.vkDel.call({ids:[inResponse.results[0]._id]});
    },
    
    
    
    // Log errors to the console for debugging
    dbFailure: function(inSender, inError, inRequest) {
        this.log(enyo.json.stringify(inError));
    }
});